<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ComposeController extends Controller
{
    public function showComposePage()
    {
        return view('pages.compose');
    }
}
