<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

    const TYPE_DOCS = [];
    const TYPE_AUDIO = [];
    const TYPE_VIDEOS = [];

    protected $guarded = ['id'];

    public function email()
    {
        return $this->belongsTo(App\Models\Email::class, 'email_id');
    }
}
