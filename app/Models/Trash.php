<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function email()
    {
        return $this->belongsTo(App\Models\Email::class, 'email_id');
    }
}
