<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    public const STATUS_PENDING = 'pending';
    public const STATUS_SENT = 'sent';
    public const STATUS_DRAFT = 'draft';
    public const STATUS_TRASHED = 'trash';

    protected $guarded = ['id'];

    public function draft()
    {
        return $this->hasOne(App\Models\Draft::class, 'email_id');
    }

    public function recipients()
    {
        return $this->belongsToMany(App\Models\User::class, 'email_user', 'email_id', 'user_id');
    }

    public function trash()
    {
        return $this->hasOne(App\Models\Trash::class, 'email_id');
    }
}
